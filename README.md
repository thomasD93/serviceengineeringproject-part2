## Service Engineering Car Rental Service
Team members: Thomas Dutka, Leon Freudenthaler, Karin Männersdorfer, Florian Niszl, Michael Trautner

### Api Gateway

The api-gateway handles HTTP requests, transforms them to the AMQP message protocol and communicates with the backend microservices.
The workflows for the use-cases are described in the responsible services.

### User service
The user-service handles registration and login by users.
User information is stored in mongoDB.

##### Login
![login](./uml/login.png)

##### Registration
![register](./uml/registration.png)

### Location service
The location-service sends out notifications of existing locations.
The information is stored in mongoDb.

##### Update
![location](./uml/location.png)

### Car service
The car-service is responsible for the car management.
It offers the possibility to change the availability of existing cars via RPC.
The car information is stored in redis.

##### Get all cars
![cars](./uml/getCars.png)


### Currency service
The currency-service regularly sends out notifications of the current currency rates retrieved from the web xml.
The currency-service does not have a database and is completely stateless.

##### Update
![currency](./uml/currency.png)

### Reservation service
The reservation-service handles adding reservations, deleting reservations and retrieving reservations.
It communicates with the car-service to change availability of cars reserved.

##### Reserve car
![reserve](./uml/reservation.png)

##### Return car
![return](./uml/return_car.png)



