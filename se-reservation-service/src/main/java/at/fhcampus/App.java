package at.fhcampus;

import at.fhcampus.messaging.AddReservationReceiver;
import at.fhcampus.messaging.DeleteReservationReceiver;
import at.fhcampus.messaging.GetReservationsReceiver;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException, TimeoutException {
        System.out.println( "Hello World!" );
        AddReservationReceiver reservationReceiver = new AddReservationReceiver();
        DeleteReservationReceiver deleteReservationReceiver = new DeleteReservationReceiver();
        GetReservationsReceiver reservationsReceiver = new GetReservationsReceiver();
        Thread thread1 = new Thread(reservationReceiver);
        Thread thread2 = new Thread(deleteReservationReceiver);
        Thread thread3 = new Thread(reservationsReceiver);
        thread1.start();
        thread2.start();
        thread3.start();
    }
}
