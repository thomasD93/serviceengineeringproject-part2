package at.fhcampus.data;

import at.fhcampus.model.Reservation;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.mongodb.client.model.Filters.eq;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class ReservationRepository {

    private Map<Integer, Reservation> reservationMap;
    private MongoClient mongoClient;
    private MongoDatabase database;
    MongoCollection<Reservation> collection;
    private static ReservationRepository instance;

    public static ReservationRepository getInstance() {
        if(instance == null ) {
            instance = new ReservationRepository();
        }

        return instance;
    }

    private ReservationRepository() {
        this.reservationMap = new HashMap<>();
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        mongoClient = new MongoClient("mongo", 27017);
        database = mongoClient.getDatabase("se-reservation-service");
        collection = database.getCollection("reservations", Reservation.class).withCodecRegistry(pojoCodecRegistry);
    }

    public void addReservation(final Reservation reservation) {
        Integer id = Math.toIntExact(collection.countDocuments()+1);
        System.out.println("Setting id " + id);
        reservation.setId(id);
        System.out.println("Adding reservation " + reservation);
        collection.insertOne(reservation);
    }

    public List<Reservation> getReservationForUser(final Integer id) {
        return collection.find(eq("userId", id)).into(new ArrayList<>());
    }

    public Integer getCarIdForReservation(final Integer id) {
        FindIterable<Reservation> id1 = collection.find(eq("_id", id)).limit(1);
        Reservation first = id1.first();
        return first != null ? first.getCarId() : null;
    }

    public void removeReservation(final Integer reservation) {
        collection.deleteOne(eq("_id", reservation));
    }

}
