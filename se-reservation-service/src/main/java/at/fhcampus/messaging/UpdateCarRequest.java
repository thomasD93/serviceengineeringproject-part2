package at.fhcampus.messaging;

public class UpdateCarRequest {
    private final String request = "update_car";
    private RequestBody requestBody;

    public UpdateCarRequest(Integer carId, boolean available) {
        this.requestBody = new RequestBody(carId, available);
    }

    public UpdateCarRequest() {
    }

    public String getRequest() {
        return request;
    }

    public RequestBody getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(RequestBody requestBody) {
        this.requestBody = requestBody;
    }

    private class RequestBody{
        private Integer carId;
        private boolean available;

        public RequestBody() {
        }

        public RequestBody(Integer carId, boolean available) {
            this.carId = carId;
            this.available = available;
        }

        public Integer getCarId() {
            return carId;
        }

        public void setCarId(Integer carId) {
            this.carId = carId;
        }

        public boolean isAvailable() {
            return available;
        }

        public void setAvailable(boolean available) {
            this.available = available;
        }
    }
}
