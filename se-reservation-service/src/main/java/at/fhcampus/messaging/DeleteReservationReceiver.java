package at.fhcampus.messaging;

import at.fhcampus.data.ReservationRepository;
import at.fhcampus.model.Reservation;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class DeleteReservationReceiver implements Runnable {

    private static final String RPC_QUEUE_NAME = "delete_reservation";
    private final CarServiceSender carServiceSender;
    private final ObjectMapper om;
    public DeleteReservationReceiver() throws IOException, TimeoutException {
        this.carServiceSender = new CarServiceSender();
        om = new ObjectMapper();
    }

    @Override
    public void run() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("rabbitmq");

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(RPC_QUEUE_NAME, false, false, false, null);
            channel.queuePurge(RPC_QUEUE_NAME);
            channel.basicQos(1);

            System.out.println(" [x] Awaiting delete reservation RPC requests");

            Object monitor = new Object();
            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                AMQP.BasicProperties replyProps = new AMQP.BasicProperties
                        .Builder()
                        .correlationId(delivery.getProperties().getCorrelationId())
                        .build();
                try {
                    String receivedMessage = new String(delivery.getBody(), "UTF-8");
                    String response = "OK";
                    Integer carId = ReservationRepository.getInstance().getCarIdForReservation(Integer.parseInt(receivedMessage));
                    UpdateCarRequest request = new UpdateCarRequest(carId, true);
                    String carResponse = carServiceSender.call(om.writeValueAsString(request));
                    if(carResponse.equalsIgnoreCase("not_ok")) {
                        response = "NOT_OK";
                    } else {
                        ReservationRepository.getInstance().removeReservation(carId);
                    }
                    System.out.println(receivedMessage);
                    channel.basicPublish("", delivery.getProperties().getReplyTo(), replyProps, response.getBytes("UTF-8"));
                } catch (RuntimeException e) {
                    e.printStackTrace();
                    System.out.println(" [.] " + e.toString());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                    // RabbitMq consumer worker thread notifies the RPC server owner thread
                    synchronized (monitor) {
                        monitor.notify();
                    }
                }
            };

            channel.basicConsume(RPC_QUEUE_NAME, false, deliverCallback, (consumerTag -> {
            }));
            // Wait and be prepared to consume the message from RPC client.
            while (true) {
                synchronized (monitor) {
                    try {
                        monitor.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}