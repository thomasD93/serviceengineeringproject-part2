package at.fhcampus.messaging;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import static org.junit.Assert.*;

public class UpdateCarRequestTest {

    @Test
    public void test() throws JsonProcessingException {
        UpdateCarRequest request = new UpdateCarRequest(1, true);
        ObjectMapper om = new ObjectMapper();
        System.out.println(om.writeValueAsString(request));
    }
}