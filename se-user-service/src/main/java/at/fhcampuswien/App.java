package at.fhcampuswien;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        UserAddedMessageReceiver receiver = new UserAddedMessageReceiver();
        Thread userAdded = new Thread(receiver);
        userAdded.start();
        LoginUserMessageReceiver loginReceiver = new LoginUserMessageReceiver();
        Thread loginUser = new Thread(loginReceiver);
        loginUser.start();
        RegisterUserMessageReceiver registerReceiver = new RegisterUserMessageReceiver();
        Thread registerUser = new Thread(registerReceiver);
        registerUser.start();
    }
}
