package at.fhcampuswien;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.*;
import org.apache.commons.lang.RandomStringUtils;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

public class LoginUserMessageReceiver implements Runnable {

    private static final String RPC_QUEUE_NAME = "post_login";

    public LoginUserMessageReceiver() {

    }

    @Override
    public void run() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("rabbitmq");
        ObjectMapper om = new ObjectMapper();

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(RPC_QUEUE_NAME, false, false, false, null);
            channel.queuePurge(RPC_QUEUE_NAME);
            channel.basicQos(1);

            System.out.println(" [x] Awaiting login RPC requests");

            Object monitor = new Object();
            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                AMQP.BasicProperties replyProps = new AMQP.BasicProperties
                        .Builder()
                        .correlationId(delivery.getProperties().getCorrelationId())
                        .build();
                try {

                    String receivedMessage = new String(delivery.getBody(), "UTF-8");
                    String response = "NOT_OK";
                    User user = om.readValue(receivedMessage, User.class);
                    Optional<User> byNameAndPassword = UserRepository.getInstance().findByNameAndPassword(user.getEmail(), user.getPassword());
                    if(byNameAndPassword.isPresent()) {
                        LoginUserResponse message = new LoginUserResponse();
                        message.setUserId(byNameAndPassword.get().getId());
                        message.setSessionCookie(RandomStringUtils.random(16, true, true));
                        response = om.writeValueAsString(message);
                    }
                    System.out.println(receivedMessage);
                    channel.basicPublish("", delivery.getProperties().getReplyTo(), replyProps, response.getBytes("UTF-8"));
                } catch (RuntimeException e) {
                    System.out.println(" [.] " + e.toString());
                } finally {
                    channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                    // RabbitMq consumer worker thread notifies the RPC server owner thread
                    synchronized (monitor) {
                        monitor.notify();
                    }
                }
            };

            channel.basicConsume(RPC_QUEUE_NAME, false, deliverCallback, (consumerTag -> {
            }));
            // Wait and be prepared to consume the message from RPC client.
            while (true) {
                synchronized (monitor) {
                    try {
                        monitor.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}