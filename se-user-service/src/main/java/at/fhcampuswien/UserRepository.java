package at.fhcampuswien;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.util.*;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class UserRepository {

    private final Map<String, User> userMap;
    private static UserRepository instance = null;
    private final MongoCollection<User> collection;

    public static UserRepository getInstance(){
        if(instance == null) {
            instance = new UserRepository();
        }
        return instance;
    }

    private UserRepository() {
        this.userMap = new HashMap<>();
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        MongoClient mongoClient = new MongoClient("mongo", 27017);
        MongoDatabase database = mongoClient.getDatabase("se-user-service");
        collection = database.getCollection("users", User.class).withCodecRegistry(pojoCodecRegistry);
    }

    public boolean addUser(User user) {
        Integer id = Math.toIntExact(collection.countDocuments()+1);
        user.setId(id);
        long exists = collection.countDocuments(eq("email", user.getEmail()));
        System.out.println(exists);
        if(exists != 0) return false;
        collection.insertOne(user);
        System.out.println("inserted " + user);
        return true;
    }

    public Optional<User> findByNameAndPassword(final String name, final String password) {
        FindIterable<User> limit = collection.find(and(eq("email", name), eq("password", password))).limit(1);
        return Optional.ofNullable(limit.first());
    }

    public List<User> getAll() {
        return collection.find().into(new ArrayList<>());
    }
}
