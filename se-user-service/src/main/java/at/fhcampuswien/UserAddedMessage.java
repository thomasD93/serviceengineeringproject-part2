package at.fhcampuswien;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserAddedMessage {

    @JsonProperty("correlationId")
    private String correlationId;
    @JsonProperty("user")
    private User user;

    public UserAddedMessage(String correlationId, User user) {
        this.correlationId = correlationId;
        this.user = user;
    }

    public UserAddedMessage() {
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public User getUser() {
        return user;
    }

    @Override
    public String toString() {
        return "{" +
                "correlationId=\"" + correlationId + '\"'  +
                ", user:" + user + "\"" +
                '}';
    }
}
