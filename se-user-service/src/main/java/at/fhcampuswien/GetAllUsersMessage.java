package at.fhcampuswien;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class GetAllUsersMessage {

    @JsonProperty("users")
    private List<User> users;

    public GetAllUsersMessage(List<User> users) {
        this.users = users;
    }

    public GetAllUsersMessage() {
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
