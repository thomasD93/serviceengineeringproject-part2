package at.fhcampuswien;

import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

public class UserRepositoryTest {

    private UserRepository sut;

    @Before
    public void setUp() {
        this.sut = UserRepository.getInstance();
    }

    @Test
    public void findByNameAndPassword_validInput_shallReturnUser() {
        User user = new User(1, "test@mail.com", "abcde");
        sut.addUser(user);

        Optional<User> actual = sut.findByNameAndPassword("test@mail.com", "abcde");

        assertTrue(actual.isPresent());
        assertEquals(user, actual.get());
    }

}