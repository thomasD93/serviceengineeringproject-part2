import redis


class RedisDB:

    # Class Attribute
    redis_host = "redis"
    redis_port = 6379
    redis_db = redis.Redis()

    # Initializer / Instance Attributes
    def __init__(self):
        self.redis_db = redis.Redis(host=self.redis_host, port=self.redis_port)
        self.redis_db.set("msg:hello", "Hello Redis!!!")

    def getData(self, key):
        return self.redis_db.get(key)

    def updateData(self, key, value):
        return self.redis_db.getset(key, value)
