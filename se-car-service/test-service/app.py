import pika
import time
import uuid
import json
import threading

class RpcTestClient(object):

    def __init__(self):
        self.credentials = pika.PlainCredentials('admin','admin')
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters("rabbitmq-server", 5672, '/', self.credentials))

        self.channel = self.connection.channel()

        result = self.channel.queue_declare('', exclusive=True)
        self.callback_queue = result.method.queue

        self.channel.basic_consume(
            queue=self.callback_queue,
            on_message_callback=self.on_response,
            auto_ack=True)

    def on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            self.response = body

    def call_price(self):
        self.response = None
        self.corr_id = str(uuid.uuid4())
        self.channel.basic_publish(
            exchange='',
            routing_key='CarManagement',
            properties=pika.BasicProperties(
                reply_to=self.callback_queue,
                correlation_id=self.corr_id,
            ),
            body=json.dumps({"request":"get_price","request-body":{"car_id":"id:1"}})
        )
        while self.response is None:
            self.connection.process_data_events()
        return self.response

    def call_get_car(self):
        self.response = None
        self.corr_id = str(uuid.uuid4())
        self.channel.basic_publish(
            exchange='',
            routing_key='CarManagement',
            properties=pika.BasicProperties(
                reply_to=self.callback_queue,
                correlation_id=self.corr_id,
            ),
            body=json.dumps({"request":"get_car","request-body":{"car_id":"id:2"}})
        )
        while self.response is None:
            self.connection.process_data_events()
        return self.response

    def call_get_car_all(self):
        self.response = None
        self.corr_id = str(uuid.uuid4())
        self.channel.basic_publish(
            exchange='',
            routing_key='CarManagement',
            properties=pika.BasicProperties(
                reply_to=self.callback_queue,
                correlation_id=self.corr_id,
            ),
            body=json.dumps({"request":"get_car","request-body":{"car_id":None}})
        )
        while self.response is None:
            self.connection.process_data_events()
        return self.response

    def call_update_car(self):
        self.response = None
        self.corr_id = str(uuid.uuid4())
        self.channel.basic_publish(
            exchange='',
            routing_key='CarManagement',
            properties=pika.BasicProperties(
                reply_to=self.callback_queue,
                correlation_id=self.corr_id,
            ),
            body=json.dumps({"request":"update_car","request-body":{"car_id":"id:2"}})
        )
        while self.response is None:
            self.connection.process_data_events()
        return self.response

    def call_invalid(self):
        self.response = None
        self.corr_id = str(uuid.uuid4())
        self.channel.basic_publish(
            exchange='',
            routing_key='CarManagement',
            properties=pika.BasicProperties(
                reply_to=self.callback_queue,
                correlation_id=self.corr_id,
            ),
            body=json.dumps({"request":"invalid_request","request-body":{"car_id":"id:2"}})
        )
        while self.response is None:
            self.connection.process_data_events()
        return self.response


class FanoutTestClient():
    def __init__(self):
        self.credentials = pika.PlainCredentials('admin','admin')
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters("rabbitmq-server", 5672, '/', self.credentials))

        self.channel = self.connection.channel()

        self.channel.exchange_declare(exchange='CarManagementFanout', exchange_type='fanout')
        result = self.channel.queue_declare('', exclusive=True)
        queue_name = result.method.queue
        self.channel.queue_bind(exchange='CarManagementFanout', queue=queue_name)

        self.channel.basic_consume(queue=queue_name, on_message_callback=self.callback_fanout, auto_ack=True)

    def run(self):
        self.channel.start_consuming()

    def callback_fanout(self, ch, method, properties, body):
        print("Fanout received: %r" % body.decode("utf-8"))


if __name__== '__main__':
    time.sleep(14)
    rpc_client = RpcTestClient()

    fanout_client = FanoutTestClient()
    threading.Thread(target=fanout_client.run).start()

    print(" [x] Requesting a lulz")

    # price request
    response = rpc_client.call_price()
    print(" [.] Got price: " + response.decode("utf-8"))

    # get all cars
    response = rpc_client.call_get_car_all()
    print(" [.] All cars: " + response.decode("utf-8"))

    # invalid request
    response = rpc_client.call_invalid()
    print(" [.] Got invalid: " + response.decode("utf-8"))

    # availability update
    response = rpc_client.call_get_car()
    print(" [.] Toggle before " + response.decode("utf-8"))
    response = rpc_client.call_update_car()
    print(" [.] Toggle " + response.decode("utf-8"))
    response = rpc_client.call_get_car()
    print(" [.] Toggle after " + response.decode("utf-8"))
