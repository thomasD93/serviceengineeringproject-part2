import redis

class RedisDB:

    # Class Attribute
    redis_host = "redis"
    redis_port = 6379

    # Initializer / Instance Attributes
    def __init__(self):
        self.redis_db = redis.Redis(host=self.redis_host, port=self.redis_port)

    # some cars to start with
    def init_some_data(self):
        self.redis_db.sadd("id_set", "id:1")
        self.redis_db.hmset("id:1", {"id":1,"companyId":1, "brand":"Mazda",
                                   "model":"3 Supersport", "available":"true",
                                   "price":122.0, "driveType":"Diesel",
                                   "image":"mazda.jpg", "description":"This is a Mazda"})
        self.redis_db.sadd("id_set", "id:2")
        self.redis_db.hmset("id:2", {"id":2,"companyId":2, "brand":"Mercedes",
                                   "model":"A Klasse", "available":"true",
                                   "price":222.0, "driveType":"Benzin",
                                   "image":"porsche.jpg", "description":"This is a Mercedes"})
        self.redis_db.sadd("id_set", "id:3")
        self.redis_db.hmset("id:3", {"id":3,"companyId":2, "brand":"Toyota",
                                   "model":"A Klasse", "available":"true",
                                   "price":202.0, "driveType":"Benzin",
                                   "image":"toyota.jpg", "description":"This is a Toyota"})

    @staticmethod
    def convert_binary_dict(data):
        return {key.decode(): val.decode() for key, val in data.items()}

    def get_data_complete(self, name):
        return self.convert_binary_dict(self.redis_db.hgetall(name))

    def get_data(self, name, key):
        return self.redis_db.hget(name, key).decode()

    def update_data(self, name, key, value):
        return self.redis_db.hset(name, key, value)

    def get_set_members(self, set_name):
        return self.redis_db.smembers(set_name)
