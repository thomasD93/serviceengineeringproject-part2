import pika
import time
from requests import *
from dblayer import *


def on_request(ch, method, props, body, redis):
    request_dict = json.loads(body.decode("utf-8"))

    if request_dict["request"] == "get_price":
        response = request_get_price(request_dict["request-body"]["car_id"], redis)

    elif request_dict["request"] == "get_car":
        response = request_get_car(request_dict["request-body"]["car_id"], redis)

    elif request_dict["request"] == "update_car":
        response = request_update_car(request_dict["request-body"]["car_id"], ["request-body"]["available"], redis)
        ch.basic_publish(exchange='CarManagementFanout', routing_key='', body=response)

    else:
        response = json.dumps([{"request":"invalid"}])

    ch.basic_publish(exchange='',
                     routing_key=props.reply_to,
                     properties=pika.BasicProperties(correlation_id = props.correlation_id),
                     body=str(response))
    ch.basic_ack(delivery_tag=method.delivery_tag)


if __name__ == '__main__':
    time.sleep(12)
    redis = RedisDB()
    redis.init_some_data()

    connection = pika.BlockingConnection(pika.ConnectionParameters("rabbitmq", 5672, '/'))
    channel = connection.channel()

    channel.queue_declare(queue='CarManagement')
    channel.exchange_declare(exchange='CarManagementFanout', exchange_type='fanout')

    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(queue='CarManagement', on_message_callback=
        lambda ch, method, props, body: on_request(ch, method, props, body, redis=redis))

    print(" [x] Awaiting RPC requests")
    channel.start_consuming()
