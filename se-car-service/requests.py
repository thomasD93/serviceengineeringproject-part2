import json
from car import *


def request_get_price(id, redis):
    car = Car(redis)
    response_json = json.dumps([{"price": car.get_price(id)}])
    return response_json

def request_get_car(id, redis):
    car = Car(redis)
    response_json = json.dumps(car.get_car(id) if id is None else [car.get_car(id)])
    return response_json

def request_update_car(id, available, redis):
    car = Car(redis)
    response_json = json.dumps(car.change_car_availability(id, available))
    return response_json
