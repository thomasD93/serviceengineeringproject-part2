from dblayer import *

class Car:

    # Initializer / Instance Attributes
    def __init__(self, redis):
        self.redis = redis

    def get_price(self, id):
        return self.redis.get_data(id, "price")

    def get_car(self, id):
        if id is None:
            return self.get_car_all()
        else:
            car_dict = {}
            car_dict.update(self.redis.get_data_complete(id))
            return car_dict

    def get_car_all(self):
        ids = self.redis.get_set_members("id_set")
        cars = []
        for id in ids:
            cars.append(self.get_car(id.decode()))
        return cars

    def toggle_car_availability(self, id):
        if self.redis.get_data(id, "available") == "true":
            self.redis.update_data(id, "available", "false")
        else:
            self.redis.update_data(id, "available", "true")
        return [self.get_car(id)]

    def change_car_availability(self, id, available):
        if self.redis.get_data(id, "available") == available:
            return "not_ok"
        else:
            self.redis.update_data(id, "available", available)
            return "ok"
