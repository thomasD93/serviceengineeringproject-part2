## Connection parameter
host: rabbitmq-server  
port: 5672  
v-host: /  
credentials: admin, admin

rpc queue = CarManagement  
fanout exchange = CarManagementFanout

## Request price
req: ```json {"request":"get_price","request-body":{"car_id":"id:1"}} ```  
res: ```json [{"price": "122.0"}] ```

## Request all cars
req: ```json{"request":"get_car","request-body":{"car_id":None}}```  
res: ```json[{"id": "1", "companyId": "1", "brand": "Mazda", "model": "3 Supersport", "available": "true", "price": "122.0", "drive_type": "Diesel", "image": "mazda.jpg", "description": "This is a Mazda"}, {"id": "3", "companyId": "2", "brand": "Toyota", "model": "A Klasse", "available": "true", "price": "202.0", "drive_type": "Benzin", "image": "toyota.jpg", "description": "This is a Toyota"}, {"id": "2", "companyId": "2", "brand": "Mercedes", "model": "A Klasse", "available": "true", "price": "222.0", "drive_type": "Benzin", "image": "porsche.jpg", "description": "This is a Mercedes"}]```

## Request car
req: ```json{"request":"get_car","request-body":{"car_id":"id:2"}}```  
res: ```json[{"id": "2", "companyId": "2", "brand": "Mercedes", "model": "A Klasse", "available": "true", "price": "222.0", "drive_type": "Benzin", "image": "porsche.jpg", "description": "This is a Mercedes"}]```

## Update car availability
req: ```json{"request":"update_car","request-body":{"car_id":"id:2"}}```  
res: ```json[{"id": "2", "companyId": "2", "brand": "Mercedes", "model": "A Klasse", "available": "false", "price": "222.0", "drive_type": "Benzin", "image": "porsche.jpg", "description": "This is a Mercedes"}]```

## Invalid call
req: ```json {"request":"invalid_request","request-body":{"car_id":"id:2"}} ```
res: ```json [{"request": "invalid"}]```