package at.fh.campuswien

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "locations")
data class Location(@JsonProperty("id")var id: Int,
                    @JsonProperty("name")var name: String,
                    @JsonProperty("address")var address: String?,
                    @JsonProperty("latitude") var latitude: Double,
                    @JsonProperty("longitude") var longitude: Double)