package at.fh.campuswien

import com.rabbitmq.client.BuiltinExchangeType
import com.rabbitmq.client.ConnectionFactory
import java.time.Duration
import java.util.*
import kotlin.concurrent.fixedRateTimer

fun main(args: Array<String>) {
    println("Hello, World")
    var location =  Location(1, "Baden bei Wien", "some address", 48.005021, 16.231769)
    LocationRepository.addToList(location)
    LocationRepository.addToList(Location(2, "Salzburg", "some Address", 47.805384, 13.051749))
    val fixedRateTimer = fixedRateTimer(name = "hello-timer",
        initialDelay = 60000, period = Duration.ofMinutes(1).toMillis()) {
        sendUpdatedList()
    }
}

private fun sendUpdatedList() {
    val conx = ConnectionFactory()
    conx.host = "rabbitmq"
    val connection = conx.newConnection()
    val channel = connection?.createChannel()
    val declareOk = channel?.exchangeDeclare("location.service", BuiltinExchangeType.FANOUT, false)

    channel?.basicPublish("location.service", "location.list", null, LocationRepository.printList()?.toByteArray())
    println("published")
    connection?.close()
}