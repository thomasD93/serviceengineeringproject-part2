package at.fh.campuswien

import com.fasterxml.jackson.databind.ObjectMapper
import com.mongodb.MongoClient
import org.litote.kmongo.KMongo
import org.litote.kmongo.getCollection

object LocationRepository{
    var locationList = mutableListOf<Location>()
    private val om = ObjectMapper()
    private val client : MongoClient = KMongo.createClient("mongo")

    init {
        val collection = client.getDatabase("se-location-service").getCollection<Location>()
    }

    fun addToList(location: Location){
        val db = client.getDatabase("se-location-service")
        val col = db.getCollection<Location>()
        col.insertOne(location)
        locationList.add(location)
    }

    fun removeFromList(location: Location){
        locationList.remove(location)
    }

    fun printList(): String? {
        val col = client.getDatabase("se-location-service").getCollection<Location>()
        val find = col.find()
        val locations = om.writeValueAsString(locationList)
        print(locations)
        return locations
    }
}