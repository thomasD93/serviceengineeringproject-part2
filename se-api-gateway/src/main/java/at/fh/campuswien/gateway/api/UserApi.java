package at.fh.campuswien.gateway.api;

import at.fh.campuswien.gateway.model.Car;
import at.fh.campuswien.gateway.model.User;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;

public interface UserApi {

    @RequestMapping(path = {"/user/{id}", "/user"}, produces = {"application/json"}, method = RequestMethod.GET)
    ResponseEntity<List<User>> getUsers(@Nullable @Valid @PathVariable Integer id);

    @RequestMapping(value = "/user", consumes = {"application/json"}, method = RequestMethod.POST)
    ResponseEntity<Void> addUser(@Valid @RequestBody User car);

    @RequestMapping(value = "/user", method = RequestMethod.PUT, consumes = {"application/json"})
    ResponseEntity<Void> updateUser(@Valid @RequestBody User car);

    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    ResponseEntity<Void> deleteUser(@Valid @PathVariable int id);
}
