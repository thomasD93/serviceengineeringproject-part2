package at.fh.campuswien.gateway.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Car {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("companyId")
    private Integer companyId;

    @JsonProperty("brand")
    private String brand;

    @JsonProperty("model")
    private String model;

    @JsonProperty("available")
    private Boolean available;

    @JsonProperty("price")
    private Double price;

    @JsonProperty("driveType")
    private String driveType;

    @JsonProperty("image")
    private String image;

    @JsonProperty("description")
    private String description;

    public Car(Integer id, Integer companyId, String brand, String model, Boolean available, Double price, String driveType, String image, String description) {
        this.id = id;
        this.companyId = companyId;
        this.brand = brand;
        this.model = model;
        this.available = available;
        this.price = price;
        this.driveType = driveType;
        this.image = image;
        this.description = description;
    }

    public Car() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDriveType() {
        return driveType;
    }

    public void setDriveType(String driveType) {
        this.driveType = driveType;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
