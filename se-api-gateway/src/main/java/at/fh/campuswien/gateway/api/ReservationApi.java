package at.fh.campuswien.gateway.api;

import at.fh.campuswien.gateway.model.Reservation;
import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

public interface ReservationApi {

    @RequestMapping(value = "/reservation",
            consumes = { "application/json" },
            method = RequestMethod.POST)
    ResponseEntity<Void> addReservation(@Valid @RequestBody Reservation body, HttpServletRequest request);


     @RequestMapping(path = {"/reservation", "/reservation/{id}"},
            produces = { "application/json" },
            method = RequestMethod.GET)
    ResponseEntity<List<Reservation>> searchReservations(HttpServletRequest request);

    @RequestMapping(path = "/reservation/{id}", method = RequestMethod.DELETE)
    ResponseEntity<Void> deleteReservation(@PathVariable(value = "id", required = true) Integer id);

}
