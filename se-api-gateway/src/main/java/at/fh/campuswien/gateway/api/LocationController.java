package at.fh.campuswien.gateway.api;

import at.fh.campuswien.gateway.LocationRepository;
import at.fh.campuswien.gateway.model.Location;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;

import javax.validation.Valid;
import java.util.List;

@Controller
public class LocationController implements LocationApi {
    @Override
    public ResponseEntity<List<Location>> getLocations(@Valid @PathVariable Integer id) {
        return new ResponseEntity<>(LocationRepository.getInstance().getLocations(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> addLocation(@Valid Location location) {
        return null;
    }

    @Override
    public ResponseEntity<Void> updateLocation(@Valid Location location) {
        return null;
    }

    @Override
    public ResponseEntity<Void> deleteLocation(@Valid int id) {
        return null;
    }
}
