package at.fh.campuswien.gateway;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class SessionRepository {

    private Map<String, Integer> sessionMap;

    private static SessionRepository instance = null;

    private SessionRepository() {
        this.sessionMap = new HashMap<>();
    }

    public static SessionRepository getInstance() {
        if(instance == null ) {
            instance = new SessionRepository();
        }

        return instance;
    }

    public void addSession(final String sessionCookie, final Integer username) {
        System.out.println("Storing session " + sessionCookie + " with id " + username);
        this.sessionMap.put(sessionCookie, username);
    }

    public Integer getIdForSession(final String sessionCookie) {
        return sessionMap.get(sessionCookie);
    }

    public Integer getIdFromRequest(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            System.out.println(cookie.getName());
            if (cookie.getName().equalsIgnoreCase("sessionid")){
                return getIdForSession(cookie.getValue());
            }
        }

        return null;
    }
}
