package at.fh.campuswien.gateway.api;

import at.fh.campuswien.gateway.*;
import at.fh.campuswien.gateway.model.User;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.timgroup.statsd.NonBlockingStatsDClient;
import com.timgroup.statsd.StatsDClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.TimeoutException;

@Controller
public class UserController implements UserApi {
    private final static StatsDClient statsDclient = new NonBlockingStatsDClient("at.campus", "127.0.0.1", 8125);

    @Override
    public ResponseEntity<List<User>> getUsers(@Nullable @Valid @PathVariable Integer id) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        try {
            System.out.println("tryblock");
            GetAllUsersRequestSender sender = new GetAllUsersRequestSender();
            String response = sender.call("getUsers");
            System.out.println("sent amqp");
            GetAllUsersMessage message = mapper.readValue(response, GetAllUsersMessage.class);
            return new ResponseEntity<>(message.getUsers(), HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<Void> addUser(@Valid @RequestBody User user) {
        Instant receivedTime = Instant.now();
        ObjectMapper mapper = new ObjectMapper();
        try {
            RegisterUserRequestSender sender = new RegisterUserRequestSender();
            String response = sender.call(mapper.writeValueAsString(user));
            if(response.equalsIgnoreCase("ok")) {
                Duration between = Duration.between(receivedTime, Instant.now());
                statsDclient.recordExecutionTime("post.user.success", between.toMillis());
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        Duration between = Duration.between(receivedTime, Instant.now());
        statsDclient.recordExecutionTime("post.user.error", between.toMillis());
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<Void> updateUser(@Valid User car) {
        return null;
    }

    @Override
    public ResponseEntity<Void> deleteUser(@Valid int id) {
        return null;
    }
}
