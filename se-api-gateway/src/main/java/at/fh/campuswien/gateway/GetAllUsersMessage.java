package at.fh.campuswien.gateway;

import at.fh.campuswien.gateway.model.User;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class GetAllUsersMessage {

    @JsonProperty("users")
    private List<User> users;

    public GetAllUsersMessage(List<User> users) {
        this.users = users;
    }

    public GetAllUsersMessage() {
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
