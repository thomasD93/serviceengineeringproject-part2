package at.fh.campuswien.gateway.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class DetailedLocation {
    @JsonProperty("location")
    private Location location;
    @JsonProperty("cars")
    private List<Car> cars;

    public DetailedLocation(Location location, List<Car> cars) {
        this.location = location;
        this.cars = cars;
    }

    public DetailedLocation() {
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }
}
