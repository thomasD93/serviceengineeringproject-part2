package at.fh.campuswien.gateway.api;

import at.fh.campuswien.gateway.model.Car;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;

public interface CarApi {

    @RequestMapping(path = {"/car/{id}", "/car"}, produces = {"application/json"}, method = RequestMethod.GET)
    ResponseEntity<List<Car>> getCars(@Valid @PathVariable Integer id);

    @RequestMapping(value = "/car", consumes = {"application/json"}, method = RequestMethod.POST)
    ResponseEntity<Void> addCar(@Valid @RequestBody Car car);

    @RequestMapping(value = "/car", method = RequestMethod.PUT, consumes = {"application/json"})
    ResponseEntity<Void> updateCar(@Valid @RequestBody Car car);

    @RequestMapping(value = "/car/{id}", method = RequestMethod.DELETE)
    ResponseEntity<Void> deleteCar(@Valid @PathVariable int id);
}
