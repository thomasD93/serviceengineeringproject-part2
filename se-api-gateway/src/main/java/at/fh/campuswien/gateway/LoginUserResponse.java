package at.fh.campuswien.gateway;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginUserResponse {
    @JsonProperty("userId")
    private Integer userId;
    @JsonProperty("cookie")
    private String sessionCookie;

    public LoginUserResponse() {
    }

    public LoginUserResponse(Integer userId, String sessionCookie) {
        this.userId = userId;
        this.sessionCookie = sessionCookie;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getSessionCookie() {
        return sessionCookie;
    }

    public void setSessionCookie(String sessionCookie) {
        this.sessionCookie = sessionCookie;
    }
}
