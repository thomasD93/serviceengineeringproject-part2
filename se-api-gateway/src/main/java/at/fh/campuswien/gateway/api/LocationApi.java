package at.fh.campuswien.gateway.api;

import at.fh.campuswien.gateway.model.Location;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;

public interface LocationApi {

    @RequestMapping(path = {"/location/{id}", "/location"}, produces = {"application/json"}, method = RequestMethod.GET)
    ResponseEntity<List<Location>> getLocations(@Nullable @Valid @PathVariable Integer id);

    @RequestMapping(value = "/location", consumes = {"application/json"}, method = RequestMethod.POST)
    ResponseEntity<Void> addLocation(@Valid @RequestBody Location location);

    @RequestMapping(value = "/location", method = RequestMethod.PUT, consumes = {"application/json"})
    ResponseEntity<Void> updateLocation(@Valid @RequestBody Location location);

    @RequestMapping(value = "/location/{id}", method = RequestMethod.DELETE)
    ResponseEntity<Void> deleteLocation(@Valid @PathVariable int id);
}
