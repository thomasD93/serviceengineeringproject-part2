package at.fh.campuswien.gateway;

import java.util.HashMap;
import java.util.Map;

public class CurrencyRepository {

    private Map<String, Double> prices;

    private static CurrencyRepository instance;

    public static CurrencyRepository getInstance() {
        if(instance == null) {
            instance = new CurrencyRepository();
        }

        return instance;
    }

    private CurrencyRepository() {
        this.prices = new HashMap<>();
    }

    public void addPrice(String currency, Double price) {
        this.prices.put(currency, price);
    }

    public Double getRateForCurrency(String currency) {
        return prices.get(currency);
    }
}
