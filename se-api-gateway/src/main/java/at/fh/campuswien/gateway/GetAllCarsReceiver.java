package at.fh.campuswien.gateway;

import at.fh.campuswien.gateway.model.Car;
import at.fh.campuswien.gateway.model.Location;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class GetAllCarsReceiver implements Runnable{

    @Override
    public void run() {
        System.out.println("Starting car list receiver");
        ObjectMapper mapper = new ObjectMapper();
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("rabbitmq");
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            channel.exchangeDeclare("CarManagementFanout", "fanout");
            String queueName = "CarManagement";
            channel.queueBind("CarManagement", "CarManagementFanout", "");

            System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String message = new String(delivery.getBody(), "UTF-8");
                System.out.println(" [x] Received '" + message + "'");
                LocationRepository.getInstance().setLocations(mapper.readValue(message, new TypeReference<List<Location>>(){}));
            };
            channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {
            });
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
