package at.fh.campuswien.gateway;

import at.fh.campuswien.gateway.model.Location;

import java.util.ArrayList;
import java.util.List;

public class LocationRepository {

    private List<Location> locations;

    private static LocationRepository instance = null;

    public static LocationRepository getInstance(){
        if(instance == null) {
            instance = new LocationRepository();
        }

        return instance;
    }

    private LocationRepository() {
        this.locations = new ArrayList<>();
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }
}
