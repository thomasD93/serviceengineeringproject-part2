package at.fh.campuswien.gateway.api;

import at.fh.campuswien.gateway.LoginUserRequestSender;
import at.fh.campuswien.gateway.LoginUserResponse;
import at.fh.campuswien.gateway.SessionRepository;
import at.fh.campuswien.gateway.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.time.Duration;
import java.util.Map;
import java.util.concurrent.TimeoutException;

@Controller
public class LoginController implements LoginApi {

    @Override
    public ResponseEntity<Void> loginUser(@RequestParam Map<String, String> valueMap, HttpServletResponse response, HttpServletRequest request) {
        long startingTime = System.currentTimeMillis();
        ObjectMapper om = new ObjectMapper();
        HttpHeaders headers = new HttpHeaders();

        try {
            LoginUserRequestSender sender = new LoginUserRequestSender();
            User user = new User(null, valueMap.get("username"), valueMap.get("password"));
            System.out.println(user);
            String loginResponse = sender.call(om.writeValueAsString(user));
            if(loginResponse.equalsIgnoreCase("NOT_OK")) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            LoginUserResponse credentials = om.readValue(loginResponse, LoginUserResponse.class);
            final Cookie cookie = new Cookie("SessionID", credentials.getSessionCookie());
            cookie.setHttpOnly(true);
            cookie.setMaxAge(30*60);
            response.addCookie(cookie);
            SessionRepository.getInstance().addSession(credentials.getSessionCookie(), credentials.getUserId());
            System.out.println(credentials);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long endTime = System.currentTimeMillis();
        System.out.println(endTime - startingTime);
        return ResponseEntity.status(HttpStatus.OK).headers(headers).build();
    }
}
