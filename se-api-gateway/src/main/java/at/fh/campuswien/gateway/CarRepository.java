package at.fh.campuswien.gateway;

import at.fh.campuswien.gateway.model.Car;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CarRepository {

    private Map<Integer, Car> carMap;

    private static CarRepository instance;

    public static CarRepository getInstance() {
        if(instance == null) {
            instance = new CarRepository();
        }

        return instance;
    }

    private CarRepository() {
        this.carMap = new HashMap<>();
        init();
    }

    public void addCar(final Car car) {
        car.setId(carMap.size() + 1);
        carMap.put(car.getId(), car);
    }

    public List<Car> getCars() {
        return new ArrayList<>(carMap.values());
    }

    private void init() {
        Car car1 = new Car(null,
                1,
                "Mazda",
                "3 Supersport",
                true,
                122.0,
                "Diesel",
                "mazda.jpg",
                "This is a mazda");
        addCar(car1);
        Car car2 = new Car(null,
                1,
                "Toyota",
                "Bla",
                true,
                211.0,
                "Diesel",
                "toyota.jpg",
                "This is a toyota");
        addCar(car2);
        Car car3 = new Car(null,
                2,
                "Merceded",
                "A Klasse",
                true,
                222.0,
                "Benzin",
                "porsche.jpg",
                "This is a Mercedes");
        addCar(car3);
    }
}
