package at.fh.campuswien.gateway.api;

import at.fh.campuswien.gateway.model.DetailedLocation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

public interface LocationManagerApi {
    @RequestMapping(path = {"/locationManager/{CR}"},
            produces = { "application/json" },
            method = RequestMethod.GET)
    ResponseEntity<List<DetailedLocation>> getAll(@PathVariable(value = "CR", required = false) String currency);
}
