package at.fh.campuswien.gateway.api;

import at.fh.campuswien.gateway.CarRepository;
import at.fh.campuswien.gateway.GetAllCarsRequest;
import at.fh.campuswien.gateway.GetCarsRequestSender;
import at.fh.campuswien.gateway.model.Car;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;

import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

@Controller
public class CarController implements CarApi {

    private final GetCarsRequestSender requestSender;
    private final ObjectMapper om;

    public CarController() throws IOException, TimeoutException {
        requestSender = new GetCarsRequestSender();
        om = new ObjectMapper();
    }

    @Override
    public ResponseEntity<List<Car>> getCars(@Nullable @Valid @PathVariable Integer id) {
        List<Car> cars = new ArrayList<>();
        try {
            String response = requestSender.call(GetAllCarsRequest.message);
            cars = om.readValue(response, new TypeReference<List<Car>>(){});
            System.out.println(cars);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(cars, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> addCar(@Valid Car car) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @Override
    public ResponseEntity<Void> updateCar(@Valid Car car) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @Override
    public ResponseEntity<Void> deleteCar(@Valid int id) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
