package at.fh.campuswien.gateway.api;

import at.fh.campuswien.gateway.AddReservationRequestSender;
import at.fh.campuswien.gateway.DeleteReservationRequestSender;
import at.fh.campuswien.gateway.GetReservationsRequestSender;
import at.fh.campuswien.gateway.SessionRepository;
import at.fh.campuswien.gateway.model.Reservation;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;

@Controller
public class ReservationController implements ReservationApi {

    private final AddReservationRequestSender addReservationRequestSender = new AddReservationRequestSender();
    private final GetReservationsRequestSender getReservationsRequestSender = new GetReservationsRequestSender();
    private final DeleteReservationRequestSender deleteReservationRequestSender = new DeleteReservationRequestSender();
    private final ObjectMapper om = new ObjectMapper();

    public ReservationController() throws IOException, TimeoutException {
    }

    @Override
    public ResponseEntity<Void> addReservation(@Valid @RequestBody Reservation body, HttpServletRequest request) {
        Integer id = getUserId(request);
        body.setUserId(id);
        try {
            addReservationRequestSender.call(om.writeValueAsString(body));
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<Reservation>> searchReservations(HttpServletRequest request) {
        List<Reservation> reservations;
        try {
            String response = getReservationsRequestSender.call(getUserId(request).toString());
            reservations = om.readValue(response, new TypeReference<List<Reservation>>(){});
            return new ResponseEntity<>(reservations, HttpStatus.OK);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
    }

    @Override
    public ResponseEntity<Void> deleteReservation(@PathVariable Integer id) {
        try {
            deleteReservationRequestSender.call(id.toString());
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    private Integer getUserId(HttpServletRequest request) {
        return SessionRepository.getInstance().getIdFromRequest(request);
    }
}
