package at.fh.campuswien.gateway;

import at.fh.campuswien.gateway.model.Location;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class CurrencyListReceiver implements Runnable {

    @Override
    public void run() {
        System.out.println("Starting currency list receiver");
        ObjectMapper mapper = new ObjectMapper();
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("rabbitmq");
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            channel.exchangeDeclare("CurrencyRates", "fanout");
            String queueName = channel.queueDeclare().getQueue();
            channel.queueBind(queueName, "CurrencyRates", "");

            System.out.println(" [*] Waiting for new currencies. To exit press CTRL+C");

            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String message = new String(delivery.getBody(), "UTF-8");
                System.out.println(" [x] Received '" + message + "'");
                JsonReader reader = Json.createReader(new StringReader(message));
                JsonObject jsonObject = reader.readObject();
                for(String key : jsonObject.keySet()) {
                    CurrencyRepository.getInstance().addPrice(key, jsonObject.getJsonNumber(key).doubleValue());
                }
            };
            channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {
            });
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
