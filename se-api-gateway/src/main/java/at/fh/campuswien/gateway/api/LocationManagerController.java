package at.fh.campuswien.gateway.api;

import at.fh.campuswien.gateway.*;
import at.fh.campuswien.gateway.model.Car;
import at.fh.campuswien.gateway.model.DetailedLocation;
import at.fh.campuswien.gateway.model.Location;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

@Controller
public class LocationManagerController implements LocationManagerApi {

    private final GetCarsRequestSender requestSender;
    private final ObjectMapper objectMapper;

    public LocationManagerController() throws IOException, TimeoutException {
        this.requestSender = new GetCarsRequestSender();
        this.objectMapper = new ObjectMapper();
    }

    @Override
    public ResponseEntity<List<DetailedLocation>> getAll(@PathVariable(value = "CR", required = false) String currency) {
        System.out.println(currency);
        List<Car> cars = new ArrayList<>();
        try {
            String call = requestSender.call(GetAllCarsRequest.message);
            cars = objectMapper.readValue(call, new TypeReference<List<Car>>(){});
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        if(cars.isEmpty()) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        List<Location> locations = LocationRepository.getInstance().getLocations();
        List<DetailedLocation> detailedLocations = new ArrayList<>();
        Double rate = CurrencyRepository.getInstance().getRateForCurrency(currency);
        List<Car> copies = new ArrayList<>();
        cars.forEach(car -> {
            Car copy = new Car(car.getId(), car.getCompanyId(), car.getBrand(), car.getModel(), car.getAvailable(), car.getPrice(), car.getDriveType(), car.getImage(), car.getDescription());
            Double price = car.getPrice();
            System.out.println(car.getPrice());
            double newPrice = price * rate;
            copy.setPrice(Math.round(newPrice*100.0)/100.0);
            System.out.println(car.getPrice());
            copies.add(copy);
        });
        locations.forEach(l -> detailedLocations.add(new DetailedLocation(l, copies.stream().filter(car -> car.getCompanyId().equals(l.getId())).collect(Collectors.toList()))));
        return new ResponseEntity<>(detailedLocations, HttpStatus.OK);
    }
}
