import xmltodict
from urllib.request import urlopen


class XMLExtractor:

    # Class Attribute
    dictCurRat = []

    # Initializer / Instance Attributes
    def __init__(self):
        self.dictCurRat = {}
        self.extractCurrencyRatesFromXML()

    def extractCurrencyRatesFromXML(self):
        #get ecb xml file
        file = urlopen("https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml")
        xmlRaw = file.read()
        file.close()
        #parse the xml to a list
        dictRatesRaw = xmltodict.parse(xmlRaw, dict_constructor=dict)
        dictRates = dictRatesRaw["gesmes:Envelope"]["Cube"]["Cube"]["Cube"]
        for index in range(len(dictRates)):
            self.dictCurRat[dictRates[index]["@currency"]] = dictRates[index]["@rate"]

    def getCurrencyRate(self,toCurrency):
        if toCurrency in self.dictCurRat or toCurrency == "EUR":
            if toCurrency == "EUR":
                return 1 / float(self.dictCurRat["USD"])
            else:
                return float(self.dictCurRat[toCurrency]) / float(self.dictCurRat["USD"])
        else:
            return -1

    def getCurrencyRates(self):
        dictCurRat = {key : self.getCurrencyRate(key) for key, val in self.dictCurRat.items()}
        dictCurRat["EUR"] = 1 / float(self.dictCurRat["USD"])
        return dictCurRat

