import pika
import time


def callback(ch, method, properties, body):
    print("Received: %r" % body.decode("utf-8"))


if __name__== '__main__':
    time.sleep(20) # waiting for everything to get setup

    credentials = pika.PlainCredentials('admin','admin')
    connection = pika.BlockingConnection(pika.ConnectionParameters("rabbitmq-server", 5672, '/', credentials))
    channel = connection.channel()
    #channel.queue_declare(queue='CurrencyRates')
    channel.exchange_declare(exchange='CurrencyRates', exchange_type='fanout')
    result = channel.queue_declare('', exclusive=True)
    queue_name = result.method.queue
    channel.queue_bind(exchange='CurrencyRates', queue=queue_name)

    channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)
    channel.start_consuming()

    """
    while True:
        channel.basic_consume(queue='CurrencyRates', on_message_callback=callback, auto_ack=True)
        channel.start_consuming()
    """