import json
import pika
import time
import threading
from xmlextractor import *


def periodicTopicUpdate(channel, ext):
    ext.extractCurrencyRatesFromXML()
    currencyRatesJSON = json.dumps(ext.getCurrencyRates())
    channel.basic_publish(exchange='CurrencyRates', routing_key='', body=currencyRatesJSON)
    # periodic topic update - every minute
    threading.Timer(60, periodicTopicUpdate, [channel, ext]).start()


if __name__ == '__main__':
    time.sleep(10)
    credentials = pika.PlainCredentials('admin', 'admin')
    connection = pika.BlockingConnection(pika.ConnectionParameters("rabbitmq", 5672, '/'))
    channel = connection.channel()
    channel.exchange_declare(exchange='CurrencyRates', exchange_type='fanout')
    #channel.queue_declare(queue='CurrencyRates')
    ext = XMLExtractor()

    periodicTopicUpdate(channel, ext)

